import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Home from './pages/home.js';
import Login from './pages/login/login.js';

const Stack = createStackNavigator();
NavBar = props => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          options={{
            title: 'My home',

            headerShown: false,
          }}>
          {props => <Login {...props} />}
        </Stack.Screen>
        <Stack.Screen
          name="Home2"
          options={{
            title: 'My hosssme2',
            headerShown: false,
          }}>
          {props => <Home {...props} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default NavBar;
