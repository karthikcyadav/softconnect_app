import React, {Component} from 'react';
import {ImageBackground, StyleSheet, View} from 'react-native';
import {Button, IconButton, Text, Title} from 'react-native-paper';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import TextInput from 'react-native-textinput-with-icons';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileNumber: '',
      password: '',
      passwordHideToggle: true,
      icon: 'eye-off',
    };

    this.handleSubmit.bind(this);
    this.handlePasswordHide.bind(this);
  }

  handlePasswordHide = () => {
    this.state.icon === 'eye-off'
      ? this.setState({icon: 'eye'})
      : this.setState({icon: 'eye-off'});
    this.setState({
      passwordHideToggle: !this.state.passwordHideToggle,
    });
  };

  handleSubmit = () => {
    console.log(this.state.mobileNumber, 'and', this.state.password);
  };

  render() {
    return (
      <>
        <View style={styles.sectionContainer}>
          <Text />
          <Text />
          <Text />
          <Text />
          <Text />
          <Text style={{fontSize: 40, fontWeight: 'bold', color: '#128496'}}>
            Social Network
          </Text>
          <Text />
          <Text style={{fontSize: 20, fontWeight: 'bold'}}>Log In</Text>
          <TextInput
            selectionColor="green"
            keyboardType="numeric"
            value={this.state.mobileNumber}
            label="Phone Number"
            onChangeText={mobileNumber => this.setState({mobileNumber})}
            blurOnSubmit
          />
          <Text />

          <View style={{flexDirection: 'row'}}>
            <View style={{flex: 4}}>
              <TextInput
                selectionColor="green"
                value={this.state.password}
                onChangeText={password => this.setState({password})}
                label="Password"
                secureTextEntry={this.state.passwordHideToggle}
                blurOnSubmit
              />
            </View>
            <View style={{flex: 1}}>
              <IconButton
                icon={this.state.icon}
                size={25}
                color="#128496"
                onPress={this.handlePasswordHide}
              />
            </View>
          </View>

          {/* <TextInput
            label="Name"
            // RTL must used when label in arabic ex: label="اللغة العربيه"
            leftIcon="thumbsup"
            leftIconType="oct"
            rippleColor="blue"
            rightIcon="react"
            rightIconType="material"
          /> */}

          <Text />
          <Button
            icon="key"
            mode="contained"
            theme={{
              roundness: 8,
              colors: {
                underlineColor: 'transparent',
              },
            }}
            onPress={this.handleSubmit}>
            Log in
          </Button>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  passwordContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#000',
    paddingBottom: 10,
  },
  inputStyle: {
    flex: 1,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
export default Login;
