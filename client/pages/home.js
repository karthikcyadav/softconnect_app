import React from 'react';
import {TextInput} from 'react-native-paper';

class Home extends React.Component {
  state = {
    text: '',
  };

  render() {
    return (
      <TextInput
        label="Email"
        value={this.state.text}
        onChangeText={text => this.setState({text})}
      />
    );
  }
}
export default Home;
